//
//  RadialGradientView.swift
//  TransparentGradient
//
//  Created by  Masoud Moharrami on 3/14/18.
//  Copyright © 2018 Masoud Moharrami. All rights reserved.
//

import UIKit

public enum UIGradientPosition : Int{
    
    case topLeft
    case topCenter
    case topRight
    
    case centerLeft
    case center
    case centerRight
    
    case bottomLeft
    case bottomCenter
    case bottomRight
    
}

@IBDesignable
class RadialGradientView: UIView {

    @IBInspectable var insideColor: UIColor = UIColor.clear
    @IBInspectable var outsideColor: UIColor = UIColor.clear
    @IBInspectable var startRadius: CGFloat = 0
    @IBInspectable var endRadius: CGFloat = 100
    @IBInspectable var centerPosition: Int = 5
    
    var centerPoint: CGPoint = CGPoint.zero
    
    override func draw(_ rect: CGRect) {
        
        let colors = [insideColor.cgColor, outsideColor.cgColor] as CFArray
        
        let gradient = CGGradient(colorsSpace: nil, colors: colors, locations: nil)
        
        switch centerPosition % 10 {
        case 1:
            centerPoint = CGPoint(x: 0, y: 0)
            break
        case 2:
            centerPoint = CGPoint(x: center.x, y: 0)
            break
        case 3:
            centerPoint = CGPoint(x: bounds.size.width, y: 0)
            break
        case 4:
            centerPoint = CGPoint(x: 0, y: bounds.size.height / 2)
            break
        case 5:
            centerPoint = center
            break
        case 6:
            centerPoint = CGPoint(x: bounds.size.width, y: bounds.size.height / 2)
            break
        case 7:
            centerPoint = CGPoint(x: 0, y: bounds.size.height)
            break
        case 8:
            centerPoint = CGPoint(x: center.x, y: bounds.size.height)
            break
        case 9:
            centerPoint = CGPoint(x: bounds.size.width, y: bounds.size.height)
            break
        default:
            break
        }
        
//        let endRadius = min(bounds.height, bounds.width) / 2
        
//        let center = CGPoint(x: bounds.size.width / 2 , y: bounds.size.height / 2)
        
        UIGraphicsGetCurrentContext()!.drawRadialGradient(gradient!, startCenter: centerPoint, startRadius: startRadius, endCenter: centerPoint, endRadius: endRadius, options: CGGradientDrawingOptions.drawsAfterEndLocation)
        
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        if (self.backgroundColor == nil) {
            self.backgroundColor = .red
        }
    }
}

