//
//  ViewController.swift
//  TransparentGradient
//
//  Created by  Masoud Moharrami on 3/14/18.
//  Copyright © 2018 Masoud Moharrami. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var gradientView: UIViewX!
    @IBOutlet weak var directionSwitch: UISwitch!
    @IBOutlet weak var directionLabel: UILabel!
    
    // MARK: - Parameters
    private var gradient: CAGradientLayer!
    var backgroundGradientLayer = CAGradientLayer()
    
    let black = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    let clear = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
    let blue = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
    let red = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    let green = #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1)
    let yellow = #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)
    let white = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    var colorArray: [[UIColor]] = [] // (color1: UIColor, UIColor)
    var currentColorArrayIndex = -1
    
    var firstColor : UIColor!
    var secondColor : UIColor!
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addGradient()
        firstColor = yellow
        secondColor = green
        
        colorArray.append([#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), #colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1)])
        colorArray.append([#colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1), #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1)])
        colorArray.append([#colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1), #colorLiteral(red: 0.1547170511, green: 0.2870070719, blue: 0.9686274529, alpha: 1)])
        colorArray.append([#colorLiteral(red: 0.1547170511, green: 0.2870070719, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)])
        colorArray.append([#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1)])
        colorArray.append([#colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1), #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)])
        colorArray.append([#colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1), #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)])
        colorArray.append([#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)])
        colorArray.append([#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1), #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)])

        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "portre.jpg")?.draw(in: self.view.bounds)
        
        if let image = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.view.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
        
//        applyGradient()
        animateGradient()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        animateGradient()
    }

    
    func animateGradient(){
        
        currentColorArrayIndex = currentColorArrayIndex == (colorArray.count - 1) ? 0: currentColorArrayIndex + 1
        
        UIView.transition(with: gradientView, duration: 5, options: [.transitionCrossDissolve], animations: {
            self.gradientView.firstColor = self.colorArray[self.currentColorArrayIndex][0]
            self.gradientView.secondColor = self.colorArray[self.currentColorArrayIndex][1]
        }) { (success) in
            self.animateGradient()
        }
    }
    
    // MARK: - Color Animation Gradient
    func applyGradient() {
        
        backgroundGradientLayer.frame = gradientView.bounds
        backgroundGradientLayer.colors = [self.firstColor, self.secondColor]
        backgroundGradientLayer.locations = [0, 1]

        let toColors = [yellow, green, blue, black, red, white]

        let gradientChangeLocation = CABasicAnimation(keyPath: "locations")
        gradientChangeLocation.duration = 1
        gradientChangeLocation.toValue = [0.0, 0.7]
        gradientChangeLocation.fillMode = kCAFillModeForwards
        gradientChangeLocation.isRemovedOnCompletion = false
        backgroundGradientLayer.add(gradientChangeLocation, forKey: "locationsChange")

        //        var cgColorsTo = [CGColor]()
        //        for color in toColors {
        //            cgColorsTo.append(color)
        //        }

        let gradientChangeColor = CABasicAnimation(keyPath: "colors")
        gradientChangeColor.duration = 1
        gradientChangeColor.toValue = toColors
        gradientChangeColor.fillMode = kCAFillModeForwards
        gradientChangeColor.isRemovedOnCompletion = false
        backgroundGradientLayer.add(gradientChangeColor, forKey: "colorChange")

        gradientView.layer.addSublayer(backgroundGradientLayer)
    }
    
    // MARK: - Linear Color Gradient
    func addGradient(){
        
        gradient = CAGradientLayer()
        gradient.frame = view.bounds
        gradient.colors = [clear,clear, blue]
        gradient.locations = [0, 0.3, 1]
        gradient.opacity = 0.7
        
        view.layer.addSublayer(gradient)
//        view.layer.mask = gradient
        
        let g2 = CAGradientLayer()
        g2.frame = view.bounds
        g2.colors = [clear,clear, green]
        g2.locations = [0, 0.6, 1]
        g2.opacity = 0.5
        
        view.layer.addSublayer(g2)
        
    }
    
    
    // MARK: - IBActions
    @IBAction func switchChanged(_ sender: Any) {
        let dirSwitch = sender as! UISwitch
        
        if dirSwitch.isOn {
            directionLabel.text = "Vertical"
            backgroundGradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            backgroundGradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
            
            gradientView.horizontalGradient = false
            
        }else{
            directionLabel.text = "Horizontal"
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            gradientView.horizontalGradient = true
            
        }
    }
}
